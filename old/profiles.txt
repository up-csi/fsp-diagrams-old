[Organization|
	members: Member\[\]
    profile: OrganizationProfile
    admin: Member\[\]
]

[Member|
	profile: MemberProfile
    username: String
    password: String
    email: String
]

[<abstract> Profile]
[OrganizationProfile]
[MemberProfile]

[Workflow]
[Task]
// Relations
[Organization] o-> 1..* [Member]

[<abstract> Profile] <:-- [OrganizationProfile]
[<abstract> Profile] <:-- [MemberProfile]

[Workflow] o-> 1..* [Task]
